class StaticPagesController < ApplicationController
  def home
    @order = current_user.orders.build if logged_in?
  end

  def faq
  end

end