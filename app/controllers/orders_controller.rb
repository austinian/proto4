class OrdersController < ApplicationController
  before_action :logged_in_user, only: [:create, :destroy]

 # def new
  #  @micropost = current_user.microposts.build
  #  @order = Order.new
  #end

  def create
    @order = current_user.orders.build(order_params)
    if @order.save
      flash[:success] = "Order created!"
      redirect_to root_url
    else
      render 'static_pages/home'
    end
  end


  def order_params
    params.require(:order).permit(:budget, :count, :paid)
  end


end
