class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.decimal :budget
      t.integer :meals
      t.boolean :paid
      t.references :user, index: true

      t.timestamps null: false

      #add_index :orders, [:user_id, :created_at]


    end
  end
end
